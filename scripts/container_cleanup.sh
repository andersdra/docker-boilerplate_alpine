#!/bin/sh

# home folder permissions
chmod -R 771 "$C_HOME"
chown -R "$C_USER:$C_USER" "$C_HOME/"

# cleanup
apk del --no-cache --purge "$(sort < /remove)"

rm -Rf /usr/share/man/*
rm -Rf /tmp/*
rm -Rf /var/log/*
find / -maxdepth 1 -name "container*.sh" -delete
