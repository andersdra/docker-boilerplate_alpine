#!/bin/sh

# create user
if [ ! "$C_USER" = root ]
then
  addgroup \
    -g "$C_GUID" "$C_USER" \
&& adduser \
  -h "/home/$C_USER" \
  -G "$C_USER" \
  -u "$C_UID" \
  -s /sbin/nologin \
  -S -D "$C_USER"
fi
